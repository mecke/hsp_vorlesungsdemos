﻿using System;

namespace FliesskommeDemo
{
    class Program
    {

        static void Main(string[] args)
        {
            // Aufgabe: Ziehen Sie von der Zahl 1.0 den Wert 0.1 solange ab, bis die Zahl negativ wird.

            #region triviale Lösung
            double zahl = 1;
            Console.WriteLine("Wert Zahl:  " + zahl);

            zahl = zahl - 0.1; // 1
            Console.WriteLine("Wert Zahl:  " + zahl);

            zahl = zahl - 0.1; // 2
            Console.WriteLine("Wert Zahl:  " + zahl);

            zahl = zahl - 0.1; // 3
            Console.WriteLine("Wert Zahl:  " + zahl);

            zahl = zahl - 0.1; // 4
            Console.WriteLine("Wert Zahl:  " + zahl);

            zahl = zahl - 0.1; // 5
            Console.WriteLine("Wert Zahl:  " + zahl);

            zahl = zahl - 0.1; // 6
            Console.WriteLine("Wert Zahl:  " + zahl);

            zahl = zahl - 0.1; // 7
            Console.WriteLine("Wert Zahl:  " + zahl);

            zahl = zahl - 0.1; // 8
            Console.WriteLine("Wert Zahl:  " + zahl);

            zahl = zahl - 0.1; // 9
            Console.WriteLine("Wert Zahl:  " + zahl);

            zahl = zahl - 0.1; // 10
            Console.WriteLine("Wert Zahl:  " + zahl);

            zahl = zahl - 0.1; // 11
            Console.WriteLine("Wert Zahl:  " + zahl);
            #endregion

            #region for Lösung
            Console.WriteLine("");
            for (double ii = 1; ii > 0; ii-=0.1)
            {
                Console.WriteLine("Wert Zahl:  " + ii);
            }
            #endregion

            #region while Lösung
            Console.WriteLine("");
            zahl = 1;
            while (zahl > 0)
            {
                zahl -= 0.1;
                Console.WriteLine("Wert Zahl:  " + zahl);
            }
            #endregion

            #region for Lösung Faktor x10
            Console.WriteLine("");
            for (double ii = 10; ii >= 0; ii -= 1)
            {
                Console.WriteLine("Wert Zahl:  " + ii);
            }
            #endregion

            Console.ReadLine();
        }
    }
}
