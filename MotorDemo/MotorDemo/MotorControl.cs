﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotorDemo
{
    class MotorControl
    {

        private void Motorsteuerung()
        {
            // Beschleunigen vom Auto
            de.meineWerkstatt.auto.Motor.GibGas();

            // Beschleunigen vom eBike
            de.meineWerkstatt.ebike.Motor.GibGas();
        }

        static void Main(string[] args)
        {
            MotorControl mc = new MotorControl();
            mc.Motorsteuerung();
            Console.ReadLine();
        }

    }
}
