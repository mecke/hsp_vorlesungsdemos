using Microsoft.VisualStudio.TestPlatform.TestHost;
using SchleifenDemo;

namespace SchleifenDemo_UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        #region SchleifendemoTest
        // Sum_A_while 
        [TestMethod]
        public void TestMethod_A1()
        {
            var result =  SchleifenKlasse.Sum_A_while(5);
            Assert.AreEqual(result, 15);
        }

        [TestMethod]
        public void TestMethod_A2()
        {
            var result = SchleifenKlasse.Sum_A_while(0);
            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void TestMethod_A3()
        {
            var result = SchleifenKlasse.Sum_A_while(1);
            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void TestMethod_A4()
        {
            var result = SchleifenKlasse.Sum_A_while(-2);
            Assert.AreEqual(result, 0);
        }

        // Sum_B_while2 
        [TestMethod]
        public void TestMethod_B1()
        {
            var result = SchleifenKlasse.Sum_B_while2(5);
            Assert.AreEqual(result, 15);
        }

        [TestMethod]
        public void TestMethod_B2()
        {
            var result = SchleifenKlasse.Sum_B_while2(0);
            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void TestMethod_B3()
        {
            var result = SchleifenKlasse.Sum_B_while2(1);
            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void TestMethod_B4()
        {
            var result = SchleifenKlasse.Sum_B_while2(-2);
            Assert.AreEqual(result, 0);
        }

        // Sum_C_for 
        [TestMethod]
        public void TestMethod_C1()
        {
            var result = SchleifenKlasse.Sum_B_while2(5);
            Assert.AreEqual(result, 15);
        }

        [TestMethod]
        public void TestMethod_C2()
        {
            var result = SchleifenKlasse.Sum_C_for(0);
            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void TestMethod_C3()
        {
            var result = SchleifenKlasse.Sum_C_for(1);
            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void TestMethod_C4()
        {
            var result = SchleifenKlasse.Sum_C_for(-2);
            Assert.AreEqual(result, 0);
        }

        // Sum_D_rek 
        [TestMethod]
        public void TestMethod_D1()
        {
            var result = SchleifenKlasse.Sum_D_rek(5);
            Assert.AreEqual(result, 15);
        }

        [TestMethod]
        public void TestMethod_D2()
        {
            var result = SchleifenKlasse.Sum_D_rek(0);
            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void TestMethod_D3()
        {
            var result = SchleifenKlasse.Sum_D_rek(1);
            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void TestMethod_D4()
        {
            var result = SchleifenKlasse.Sum_D_rek(-2);
            Assert.AreEqual(result, 0);
        }

        // Sum_E_doWhile
        [TestMethod]
        public void TestMethod_E1()
        {
            var result = SchleifenKlasse.Sum_E_doWhile(5);
            Assert.AreEqual(result, 15);
        }

        [TestMethod]
        public void TestMethod_E2()
        {
            var result = SchleifenKlasse.Sum_E_doWhile(0);
            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void TestMethod_E3()
        {
            var result = SchleifenKlasse.Sum_E_doWhile(1);
            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void TestMethod_E4()
        {
            var result = SchleifenKlasse.Sum_E_doWhile(-2);
            Assert.AreEqual(result, 0);
        }

        #endregion

        #region SchleifendemoTest
        /* 
         * 
        // Sum_A_while 

        [TestMethod]
        public void Blanko_TestMethod_A1()
        {
            var result = Schleifendemo_blanko.BlankoProgram.Sum_A_while(5);
            Assert.AreEqual(result, 15);
        }

        [TestMethod]
        public void Blanko_TestMethod_A2()
        {
            var result = Schleifendemo_blanko.BlankoProgram.Sum_A_while(0);
            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void Blanko_TestMethod_A3()
        {
            var result = Schleifendemo_blanko.BlankoProgram.Sum_A_while(1);
            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void Blanko_TestMethod_A4()
        {
            var result = Schleifendemo_blanko.BlankoProgram.Sum_A_while(-2);
            Assert.AreEqual(result, 0);
        }


        // Sum_B_while2 
        [TestMethod]
        public void Blanko_TestMethod_B1()
        {
            var result = Schleifendemo_blanko.BlankoProgram.Sum_B_while2(5);
            Assert.AreEqual(result, 15);
        }

        [TestMethod]
        public void Blanko_TestMethod_B2()
        {
            var result = Schleifendemo_blanko.BlankoProgram.Sum_B_while2(0);
            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void Blanko_TestMethod_B3()
        {
            var result = Schleifendemo_blanko.BlankoProgram.Sum_B_while2(1);
            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void Blanko_TestMethod_B4()
        {
            var result = Schleifendemo_blanko.BlankoProgram.Sum_B_while2(-2);
            Assert.AreEqual(result, 0);
        }

        // Sum_C_for 
        [TestMethod]
        public void Blanko_TestMethod_C1()
        {
            var result = Schleifendemo_blanko.BlankoProgram.Sum_B_while2(5);
            Assert.AreEqual(result, 15);
        }

        [TestMethod]
        public void Blanko_TestMethod_C2()
        {
            var result = Schleifendemo_blanko.BlankoProgram.Sum_C_for(0);
            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void Blanko_TestMethod_C3()
        {
            var result = Schleifendemo_blanko.BlankoProgram.Sum_C_for(1);
            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void Blanko_TestMethod_C4()
        {
            var result = Schleifendemo_blanko.BlankoProgram.Sum_C_for(-2);
            Assert.AreEqual(result, 0);
        }

        // Sum_D_rek 
        [TestMethod]
        public void Blanko_TestMethod_D1()
        {
            var result = Schleifendemo_blanko.BlankoProgram.Sum_D_rek(5);
            Assert.AreEqual(result, 15);
        }

        [TestMethod]
        public void Blanko_TestMethod_D2()
        {
            var result = Schleifendemo_blanko.BlankoProgram.Sum_D_rek(0);
            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void Blanko_TestMethod_D3()
        {
            var result = Schleifendemo_blanko.BlankoProgram.Sum_D_rek(1);
            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void Blanko_TestMethod_D4()
        {
            var result = Schleifendemo_blanko.BlankoProgram.Sum_D_rek(-2);
            Assert.AreEqual(result, 0);
        }

        /*
         * 
        // Sum_E_doWhile
        [TestMethod]
        public void TestMethod_E1()
        {
            var result = SchleifenKlasse.Sum_E_doWhile(5);
            Assert.AreEqual(result, 15);
        }

        [TestMethod]
        public void TestMethod_E2()
        {
            var result = SchleifenKlasse.Sum_E_doWhile(0);
            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void TestMethod_E3()
        {
            var result = SchleifenKlasse.Sum_E_doWhile(1);
            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void TestMethod_E4()
        {
            var result = SchleifenKlasse.Sum_E_doWhile(-2);
            Assert.AreEqual(result, 0);
        }


        */
        #endregion

    }
}