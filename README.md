# VorlesungsDemos zur HSP / Mecke

Sie haben/werden in der Vorlesung verschiedene Beispiele und Demos gesehen. Diese sind in diesem Repository zusammengefasst.

VL3:
SummenDemo

VL4: 
DatentypenDemo
BasicArithmethik
FliesskommaDemo
FunktionenDemo

VL5:
SummandenDemo
MessageboxDemo

VL6: 
SchleifenDemo

VL7:
MotorDemo
KreisDemo

VL8: 
HelloGUI_Demo
KreisDemo
Console2WPF_Demo
Student_GUI_Demo
FreeGUI_Demo

VL10:
FormDemo
TryCatchDemo

VL11:
ReferenzDatentypDemo
CollectionsDemo
