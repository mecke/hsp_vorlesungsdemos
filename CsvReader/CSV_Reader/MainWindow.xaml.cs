﻿using Microsoft.Win32;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CSV_Reader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            String mesg = string.Empty;
            List<Sample> samples = new List<Sample>();

            // # Datei auswählen

            // Configure open file dialog box
            var dialog = new Microsoft.Win32.OpenFileDialog();                          
            dialog.Title = "Wählen Sie eine Datei";                                     
            dialog.InitialDirectory = @"E:\Workspaces\VS\24SS\CSV_Reader\CSV_Reader";   // anpassen
            dialog.FileName = "csv Datei"; // Default file name
            dialog.DefaultExt = ".csv"; // Default file extension
            dialog.Filter = "csv Dateien (*.csv)|*.csv|Text Dateien (*.txt)|*.txt|Alle Dateien (*.*)|*.*"; // Filter files by extension

            // Show open file dialog box
            bool? result = dialog.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // # Ergebnis ok => dann Datei lesen

                // Open document
                string filepath = dialog.FileName;
                Stream fileStream = dialog.OpenFile();
                String line;
                string[] tokens;
                double value1 = 0;
                double value2 = 0;
                double value3 = 0;

                using (StreamReader reader = new StreamReader(fileStream))
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        tokens = line.Split(';');
                        if (Double.TryParse(tokens[0], out value1) &&
                            Double.TryParse(tokens[1], out value2) &&
                            Double.TryParse(tokens[2], out value3))
                        {
                            samples.Add(new Sample()
                            {
                                Speed = value1,
                                Torque = value2,
                                Time = value3,
                            });
                        }
                        else
                        {
                            mesg += line + "\n";
                        }
                    }
                    MessageBox.Show(mesg, "File Content at path: " + filepath,
                        MessageBoxButton.OK);
                }

                // # in Datengrid einfügen
                dataGridvonKai.ItemsSource = samples;

            } 

        } // method

    } // class
} // namespace