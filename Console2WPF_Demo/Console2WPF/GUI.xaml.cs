﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Console2WPF
{
    /// <summary>
    /// Interaktionslogik für GUI.xaml
    /// </summary>
    public partial class GUI : UserControl
    {
        // Konstruktor für das UserControl
        public GUI() 
        {
            InitializeComponent();
        }

        // Schließen des Fensters
        private void btn_Exit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        // Funktion um Text verschinden zu lassen
        private void btn_verschwinde_Click(object sender, RoutedEventArgs e)
        {
            lab_Begruessung.Visibility = Visibility.Hidden;
        }

    }
}
