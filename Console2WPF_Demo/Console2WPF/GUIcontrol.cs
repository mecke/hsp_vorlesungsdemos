﻿using System;
using System.Windows;

namespace Console2WPF
{
    class GUIcontrol
    {
        /* Vorgehen
        1. Einfügen der Bibliotheken
        2. Einbinden der Namespaces
        3. Aktivieren von WPF über die *.csproj Datei
        4. Klasse eines UserControls
        5. Fenster instanziieren
        6. UC hineinlegen
        7. STAThread label hinzufügen
        8. Testen, Testen, Testen
        */

        GUIcontrol()
        {
            // Fenster erzeugen und Eigenschaften modifizieren
            Window fenster = new Window();
            fenster.ResizeMode = ResizeMode.NoResize;
            fenster.Height = 300;
            fenster.Width = 400;

            // Interface erzeugen und Eigenschaften modifizieren
            GUI meineGUI = new GUI();
            meineGUI.lab_Begruessung.Content = "Tschüss Welt";

            // Interface im Content des Fensters plazieren
            fenster.Content = meineGUI;

            // Das Fenster zeigen
            fenster.ShowDialog();
        }

        // Main Methode als Single Thread Anwendung (STAThread)
        // d.h. diese läuft nur auf einem CPU Kern
        [STAThread]
        static void Main(string[] args)
        {
            // Instanziierung 
            new GUIcontrol();
        }
    }
}
