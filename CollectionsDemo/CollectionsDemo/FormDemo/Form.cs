﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace FormDemo
{
    public abstract class Form
    {
        protected Color farbe = Color.White;

        public void setFarbe(Color lokaleFarbe )
        {
            farbe = lokaleFarbe;
        }

        internal string getFarbe()
        {
            return farbe.ToString();
        }

        abstract public double getFlaeche();

    }
}
