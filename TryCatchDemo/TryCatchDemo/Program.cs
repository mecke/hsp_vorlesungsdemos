﻿using System;

namespace TryCatchDemo
{
    class Program
    {
        Program()
        {
            // Konstruktor
            try
            {
                try
                {
                    // Versuche gefährlichen Code-Block auszuführen
                    String zahlStr = "1,123 Hallo";
                    Double zahl = Convert.ToDouble(zahlStr);
                    Console.WriteLine("Alles gut! Zahl: " + zahl);
                }
                catch (Exception ex)
                {
                    // Wenn der Code-Block scheitert, dann mache dies hier:
                    Console.WriteLine("Mist - Fehler bei der Konvertierung - Ebene 1");
                    Console.WriteLine(ex.Message);

                    // Eigener Ausnahmefehler werfen (d.h. erzeugen und aktivieren)
                    throw new Exception("Mist - Fehler bei der Konvertierung - Ebene 2");
                }
            }
            catch (Exception ex2)
            {
                // Zweite Ebene für Ausnahmefehler
                Console.WriteLine(ex2.Message);
            }
            finally
            {
                // optionaler, "finaler" Code-Block, wenn der immer nach try/catch ausgeführt wird
                Console.ReadLine();
            }

        }

        static void Main(string[] args)
        {
            // Objekt erzeugen
            new Program();
        }

    }
}
